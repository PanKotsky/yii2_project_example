<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 24.07.2017
 * Time: 14:28
 */

namespace app\components;

use app\models\Profile;
use yii\web\NotFoundHttpException;


    /*
     * function return data from profile of user and link to avatar
     * param - user id (integer)
     */

trait UserProfile
{

    public function getUser($user_id){
        if($user_id) {
            $profile = $this->finder->findProfileById($user_id);
            $avatar = Profile::getDefaultAvatar($user_id);
            return array($profile, $avatar);
        }
        else{
            throw new NotFoundHttpException();
        }
    }
}