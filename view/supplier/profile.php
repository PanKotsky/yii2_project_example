<?php
/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 26.06.2017
 * Time: 17:17
 */

use yii\helpers\Html;
use app\components\OwnToursWidget;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use \yii\helpers\Url;
//use app\components\OwnToursWidget;
?>

<?php $this->title = empty($supplierProfile->firstname && $supplierProfile->lastname) ? Html::encode($supplierProfile->username) : Html::encode($supplierProfile->firstname) . ' ' . Html::encode($supplierProfile->lastname);
$this->params['breadcrumbs'][] = $this->title; ?>


<?= $this->render('../_menu') ?>



<div class="l-profilePage__rightPart">
    <div class="c-supplierProfile__title">
        Added tours
    </div>
    <div class="c-supplierProfile__buttonWrapper">
        <?= Html::a('Add tour', ['tour/create'], ['class' => 'c-button c-button--noneTransform']); ?>
    </div>


    <?= OwnToursWidget::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_list',
        'page'  => 'supp'
    ]); ?>
</div>
