<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
//use app\assets\TempAsset;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<!--<div class="post">-->
<?php $this->registerCssFile('/css/temp.css'); ?>
<?php $this->registerCssFile('/css/bookings_temp.css'); ?>

    <?php
        $duration['days'] = idate('d', $model['duration']);
        $duration['hours'] = idate('h', $model['duration']);
        $duration['minutes'] = idate('i', $model['duration']);
    ?>

<div class="c-supplierProfile__item">

    <!-- If current page is "Supplier Profile" - display this block-->
    <?php if($page == 'supp') : ?>
    <div class="c-supplierProfile__header">
        <div class="c-supplierProfile__header--leftPart">
            Tour Rating
            <div class="c-rating">
                <div class="c-rating__stars">
                    <div class="c-rating__star">
                        <img src="/img/icon/starLightBlue.svg"  class="c-rating__icon">
                    </div>
                    <div class="c-rating__star">
                        <img src="/img/icon/starLightBlue.svg"  class="c-rating__icon">
                    </div>
                    <div class="c-rating__star">
                        <img src="/img/icon/starLightBlue.svg"  class="c-rating__icon">
                    </div>
                    <div class="c-rating__star">
                        <img src="/img/icon/starGrey.svg"  class="c-rating__icon">
                    </div>
                    <img src="/img/icon/starGrey.svg"  class="c-rating__icon">
                </div>
                <div class="c-rating__value">(<?= round($model['rating'], 1) ?>)</div>
            </div>
            <div class="c-supplierProfile__header--numberBlock">
                <?php if($model['reviews'] == 0) : ?>
                    <a href="/" class="c-supplierProfile__textBlock">
                <?php else : ?>
                    <a href="/" class="c-supplierProfile__textBlock is-active">
                <?php endif; ?>

                    <span class="c-supplierProfile__number">
                        <?= $model['reviews'] ?>
                    </span>
                    <span class="c-supplierProfile__text">
                        Review(s)
                    </span>
                </a>
               <?php if($model['messages'] == 0) : ?>
                    <a href="/" class="c-supplierProfile__textBlock">
               <?php else : ?>
                    <a href="/" class="c-supplierProfile__textBlock is-active">
               <?php endif; ?>
                    <span class="c-supplierProfile__number">
                        <?= $model['messages'] ?>
                    </span>
                    <span class="c-supplierProfile__text">
                        Message(s)
                    </span>
               </a>
               <?php if($model['orders'] == 0) : ?>
                    <a href="/" class="c-supplierProfile__textBlock">
               <?php else : ?>
                    <a href="/supplier/tour-orders?id=<?=$model['id']?>" class="c-supplierProfile__textBlock is-active">
               <?php endif; ?>
                    <span class="c-supplierProfile__number">
                        <?= $model['orders'] ?>
                    </span>
                    <span class="c-supplierProfile__text">
                        Order(s)
                    </span>
                </a>
            </div>
        </div>
        <a href="/" class="c-supplierProfile__header--rightPart">
            <svg class="c-supplierProfile__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 268.725 268.725">
                <path d="M161.359,56.337c-7.041-7.049-18.458-7.049-25.498,0l-6.374,6.381   l-89.243,89.337l0.023,0.023l-2.812,2.82c0,0-8.968,9.032-29.216,74.399c-0.142,0.457-0.283,0.911-0.426,1.374   c-0.361,1.171-0.726,2.361-1.094,3.567c-0.326,1.066-0.656,2.154-0.987,3.249c-0.279,0.923-0.556,1.836-0.839,2.779   c-0.642,2.14-1.292,4.318-1.955,6.567c-1.455,4.937-5.009,16.07-0.99,20.1c3.87,3.882,15.12,0.467,20.043-0.993   c2.232-0.662,4.395-1.311,6.519-1.952c0.981-0.296,1.932-0.586,2.891-0.878c1.031-0.314,2.057-0.626,3.062-0.935   c1.269-0.39,2.52-0.775,3.75-1.157c0.367-0.114,0.727-0.227,1.091-0.34c62.192-19.365,73.357-28.453,74.285-29.284   c0.007-0.005,0.007-0.005,0.012-0.01c0.039-0.036,0.066-0.06,0.066-0.06l2.879-2.886l0.193,0.193l89.245-89.337l-0.001-0.001   l6.374-6.381c7.041-7.048,7.041-18.476,0-25.525L161.359,56.337z M103.399,219.782c-0.078,0.053-0.184,0.122-0.296,0.193   c-0.062,0.04-0.137,0.087-0.211,0.133c-0.075,0.047-0.157,0.098-0.244,0.151c-0.077,0.047-0.157,0.095-0.243,0.147   c-2.969,1.777-11.682,6.362-32.828,14.017c-2.471,0.894-5.162,1.842-7.981,2.819l-30.06-30.091c0.98-2.84,1.929-5.551,2.826-8.041   c7.638-21.235,12.219-29.974,13.986-32.939c0.043-0.071,0.082-0.136,0.121-0.2c0.062-0.102,0.12-0.197,0.174-0.284   c0.043-0.069,0.088-0.141,0.126-0.2c0.071-0.111,0.14-0.217,0.193-0.296l2.2-2.206l54.485,54.542L103.399,219.782z M263.351,56.337   l-50.997-51.05c-7.041-7.048-18.456-7.048-25.498,0l-12.748,12.763c-7.041,7.048-7.041,18.476,0,25.524l50.996,51.05   c7.04,7.048,18.457,7.048,25.498,0l12.749-12.762C270.392,74.813,270.392,63.385,263.351,56.337z"/>
            </svg>
            Edit
        </a>
    </div>

        <!-- If current page is "profile/bookings" - display this block-->
    <?php elseif($page == 'bookings') : ?>
        <?php if($model) : ?>
        <div class="c-bookings__header">
            <div class="c-bookings__header--leftPart">
                Booking date:
                <span class="c-bookings__date">
                       <?php $dateBuy = strtotime($model['dateBuy']);
                       echo date('d-m-Y', $dateBuy)?>
                </span>
            </div>
            <div class="c-bookings__header--rightPart">
                <a href="#" class="c-bookings__download">
                    <svg class="c-bookings__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.329 43.329">
                        <path d="M18.689,30.359c1.645,1.639,4.305,1.639,5.951,0l14.082-14.014c0.91-0.906,1.186-2.287,0.695-3.474
s-1.646-1.976-2.932-1.976h-5.879V3.196C30.606,1.445,29.17,0,27.419,0H15.908C14.156,0,12.7,1.444,12.7,3.196v7.698H6.842
c-1.284,0-2.441,0.79-2.931,1.976c-0.49,1.187-0.216,2.561,0.694,3.466L18.689,30.359z"/>
                        <path d="M42.657,37.547c0-1.75-1.419-3.171-3.172-3.171H3.842c-1.751,0-3.171,1.419-3.171,3.171v2.611
c0,1.751,1.42,3.171,3.171,3.171h35.645c1.751,0,3.171-1.42,3.171-3.171L42.657,37.547L42.657,37.547z"/>
                    </svg>
                    Download the voucher
                </a>
                <a href="#" class="c-bookings__chat">
                    <svg class="c-bookings__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
                        <path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986
c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/>
                        <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8
L13.684,2.271z"/>
                        <polygon points="0,2.878 0,11.186 4.833,7.079"/>
                        <polygon points="9.167,7.079 14,11.186 14,2.875"/>
                    </svg>
                    Chat to supplier
                </a>
            </div>
        </div>
        <?php endif; ?>
    <?php endif; ?>

<div class="c-tour">
        <div class="c-tour__content cf">
            <div class="c-tour__header">
                <img src="<?=Url::to(['/uploads/photos/'.$model['src']])?>" class="c-tour__img">
                <div class="c-tour__bg"></div>
            </div>
            <div class="c-tour__body">
                <a href="<?=Url::to(['/tour/view/', 'id' => $model['id']])?>" class="c-tour__head"><?= Html::encode($model['name']) ?>
                </a>
                <div class="c-tour__text"><?= Html::encode($model['descShort']) ?>
                    <a href="<?=Url::to(['/tour/view/', 'id' => $model['id']])?>" class="c-tour__readMore">
                        Read more ›
                    </a>
                </div>
                <div class="c-tour__info cf">
                    <div class="c-tour__leftPart">
                        <div class="c-time">
                            <div class="c-time__img">
                                <svg class="c-time__icon"
                                     xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 100 100">
                                    <path d="M49.9 11C28.4 11 11 28.4 11 49.9c0 21.4 17.4 38.9 38.9 38.9 21.4 0 38.9-17.4 38.9-38.9C88.7 28.4 71.3 11 49.9 11zm0 69.5c-16.9 0-30.6-13.7-30.6-30.6C19.3 33 33 19.3 49.9 19.3c16.9 0 30.6 13.7 30.6 30.6 0 16.8-13.8 30.6-30.6 30.6zm0 0"></path>
                                    <path d="M70.1 48.7H52.7v-21c0-1.8-1.4-3.2-3.2-3.2-1.8 0-3.2 1.4-3.2 3.2v24.2c0 1.8 1.4 3.2 3.2 3.2h20.7c1.8 0 3.2-1.4 3.2-3.2-.1-1.8-1.5-3.2-3.3-3.2zm0 0"></path>
                                </svg>
                            </div>
                            <div class="c-time__text">Duration:</div>
                            <div class="c-time__clock"><?= $duration['days']?> d <?= $duration['hours']?> h <?= $duration['minutes']?> m</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-tour__footer">
                <div class="c-tour__priceBlock">
                    <div class="c-tour__textRight">Price from</div>
                    <div class="c-tour__price">$<?= $model['priceAdult'] ?></div>
                </div>
                <div class="c-tour__bookBlock">
                    <div class="c-tour__text c-tour__text--lightColor">Book with
                    </div>
                    <div class="c-tour__book">$<?= $model['priceAdult']*0.2 ?></div>
                </div>
                <div class="c-tour__buttonWrap">
                    <a href="<?=Url::to(['/tour/view/', 'id' => $model['id']])?>" class="c-button c-button--tourDetails">Details</a>
                </div>
            </div>
        </div>
    </div>
</div>


