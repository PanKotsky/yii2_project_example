<?php
/**
 * Created by PhpStorm.
 * User: Java-PC
 * Date: 27.04.2017
 * Time: 10:55
 */

namespace app\controllers;

use app\models\Cities;
use app\models\ProviderLocation;
use app\models\SupplierForm;
use app\models\UploadAvatarForm;
use app\models\User;
use app\models\Providers;
use dektrium\user\controllers\SettingsController as BaseSettingsController;
use app\models\SettingsForm;
use app\models\Profile;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Countries;
use app\components\UserProfile;
use Yii;


class SettingsController extends BaseSettingsController
{
    use UserProfile;

    /*
     * action for view profile (if profile is own of user)
     */

    public $layout;

    public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post'],
                    'delete'     => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['profile', 'account', 'networks', 'disconnect', 'delete'],
                        'roles'   => ['@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['confirm'],
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['update-avatar'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionProfile()
    {
        $this->layout = 'main';

        $userId = \Yii::$app->user->identity->getId();

        $isSupplier = false;
        $supplier = null;
        $city = null;
        $country = null;

        if(Yii::$app->user->can('updateProfile')) {
            $isSupplier = true;

            $supplier = Providers::find()->with(['providerDocs', 'providerLocations'])->where(['userId' => $userId])->one();
            $city = new Cities();
            $country = new Countries();
        }
        $settings = \Yii::createObject(SettingsForm::className());
        $upload = new UploadAvatarForm();

        $event_settings = $this->getFormEvent($settings);

        $this->performAjaxValidation($settings);
        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event_settings);

        /* винесено в  трейт */
        $user = $this->getUser($userId);

        $this->view->params['userdata'] = $user[0];
        $this->view->params['avatar'] = $user[1];
        $this->view->params['isSupplier'] = $isSupplier;
        $this->view->params['upload'] = $upload;
        $model = $user[0];

        if ($model == null) {
            $model = \Yii::createObject(Profile::className());
            $model->link('user', \Yii::$app->user->identity);
        }

        $event = $this->getProfileEvent($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);
        if($isSupplier) {
            if($supplier->load(\Yii::$app->request->post())) {
                $supplier->save();

                /* якщо юзер є саплаєром і в нього є додаткові поля налаштування профілю саплаєра */
                if ($city->load(\Yii::$app->request->post()) && $country->load(\Yii::$app->request->post())) {
                    if (($supplier->countries['name'] != $country->name) || ($supplier->cities['name'] != $city->name)) {
                        ProviderLocation::editProviderLocation($userId, $supplier->countries, $supplier->cities, $country, $city);
                    }
                }
            }
        }


        if ($model->load(\Yii::$app->request->post()) && $settings->load(\Yii::$app->request->post())) {

            $model->save();
            if($settings->load(\Yii::$app->request->post())) {
                $settings->save();
            }
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Your profile has been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event_settings);
            return $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model,
            'settings' => $settings,
            'isSupplier' => $isSupplier,
            'supplier' => $supplier,
            'city' => $city,
            'country' => $country
        ]);
    }

    public function actionUpdateAvatar(){
        $userId = \Yii::$app->user->identity->getId();
        $upload = new UploadAvatarForm();
        if(Yii::$app->request->isAjax){
            $upload->uploadImageUser = UploadedFile::getInstanceByName('file');

            if(!$src = $upload->upload()){

            }
            $userProfile = Profile::findOne(['user_id' => $userId]);

            $userProfile->src_avatar = $src;
            $userProfile->save();

            return '/uploads/usr/'.$userId.'/'.$userProfile->src_avatar;
        }
        return false;
    }

}