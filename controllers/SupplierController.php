<?php

namespace app\controllers;

use app\models\Profile;
use app\models\SupplierForm;
use app\models\Providers;
use app\models\Tour;
use Yii;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use dektrium\user\Finder;
use yii\web\Controller;
use app\rbac\SupplierRule;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\components\UserProfile;

class SupplierController extends Controller
{
    use UserProfile;

    public $layout;

    public function beforeAction($action) {
        if($action->id == 'index'){
            if(Yii::$app->user->isGuest){
                return $this->actionIndex();
            }
            else{
                $newAction = 'create';
                return parent::beforeAction($newAction);
            }
        }
        return true;
    }

    public function behaviors()
    {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['profile'],
                'rules' => [
                    [
                        'actions' => ['profile'],
                        'allow' => true,
                        'roles' => ['supplier', 'admin'],
                    ],
                ],
            ],
        ];
    }

    /*
    * if user are not authorized - resend to information page about supplier
    *
    * else - to Supplier registration page
    */

    public function actionIndex() {
        if(Yii::$app->user->isGuest){
            return $this->render('info');
        }
        elseif (Providers::find()->where(['userId' => \Yii::$app->user->identity->getId()])->asArray()->one()){
            Yii::$app->session->setFlash('warning', 'You have already submitted an application. Please wait for review.');
            return $this->render('info');
        }
        else{
            return $this->actionCreate();
        }

    }

    public function actionCreate() {

        $supplier = new SupplierForm();

        if ($supplier->load(Yii::$app->request->post())) {

            $supplier->document = UploadedFile::getInstances($supplier, 'document');

            if(!$supplier->upload()) {
                /* ToDo add Exception if don't save files */
            }
            $supplier->userId = Yii::$app->user->identity->getId();

            if($supplier->save()){
                return $this->goBack();
            }
            return $this->goBack();
        }
        return $this->render('index', [
            'supplier' => $supplier,
        ]);

    }

    public function actionProfile() {
        $this->layout = 'userblock';
        $isSupplier = false;
        $userId = Yii::$app->user->identity->getId();

        $supplier = Providers::findOne($userId);

        $user = $this->getUser($userId);

        if(Yii::$app->user->can('updateProfile')) {
            $isSupplier = true;
        }



        $subQuery = (new Query())
            ->select(['tourId', 'src', 'isPromo'])
            ->from(['tourPhotos'])
            ->where(['isPromo' => 1]);

        $query = (new Query())
            ->select(['t.*', 'ratings.tourId', 'AVG(ratings.value) AS rating', 'o.*', 'COUNT(o.orderId) AS orders', 'COUNT(m.id) AS messages', 'COUNT(rv.id) AS reviews'])
            ->from(['t' => 'tour'])
            ->join('LEFT JOIN', (['r' => 'ratings']), 'r.tourId = t.id')
            ->join('LEFT JOIN', (['tp' => $subQuery]), 'tp.tourId = t.id')
            ->join('LEFT JOIN', (['o' => 'orders']), 'o.tourId = t.id')
            ->join('LEFT JOIN', (['m' => 'messages']), 'm.tourId = t.id')
            ->join('LEFT JOIN', (['rv' => 'reviews']), 'rv.tourId = t.id')

            ->where(['t.providerId' => $userId])
            ->groupBy(['t.id']);



        $dataProvider = new ActiveDataProvider([

            'query' => $query,
            'sort' => [
                'defaultOrder' => [
//                    'user_id' => SORT_DESC
//                    'id' => SORT_DESC
//                    'title' => SORT_ASC,
                ]
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);


        $this->view->params['userdata'] = $user[0];
        $this->view->params['avatar'] = $user[1];
        $this->view->params['isSupplier'] = $isSupplier;

        return $this->render('profile', [
            'supplier' => $supplier,
            'supplierProfile' => $user[0],
            'isSupplier' => $isSupplier,
            'dataProvider' => $dataProvider
        ]);

    }
}
