<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "providers".
 *
 * @property integer $userId
 * @property string $nameProvider
 * @property string $regNumberProvider
 * @property string $descProvider
 * @property string $addressProvider
 * @property string $zipProvider
 * @property string $phoneProvider
 * @property string $emailProvider
 *
 * @property Orders[] $orders
 * @property ProviderDocs[] $providerDocs
 * @property ProviderLocation[] $providerLocations
 * @property Profile $user
 * @property User $user0
 * @property Tour[] $tours
 */
class Providers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'nameProvider', 'emailProvider'], 'required'],
            [['userId'], 'integer'],
            [['descProvider'], 'string'],
            [['nameProvider', 'regNumberProvider', 'addressProvider', 'emailProvider'], 'string', 'max' => 255],
            [['zipProvider'], 'string', 'max' => 20],
            [['phoneProvider'], 'string', 'max' => 25],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['userId' => 'user_id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'nameProvider' => 'Name Provider',
            'regNumberProvider' => 'Reg Number Provider',
            'descProvider' => 'Desc Provider',
            'addressProvider' => 'Address Provider',
            'zipProvider' => 'Zip Provider',
            'phoneProvider' => 'Phone Provider',
            'emailProvider' => 'Email Provider',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['providerId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderDocs()
    {
        return $this->hasMany(ProviderDocs::className(), ['userId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProviderLocations()
    {
        return $this->hasMany(ProviderLocation::className(), ['userId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTours()
    {
        return $this->hasMany(Tour::className(), ['providerId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cityId'])->via('providerLocations');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasOne(Countries::className(), ['id' => 'countryId'])->via('providerLocations');
    }
}
